---
title: Screenshots
layout: page
2024-screenshots:
  - url: /screenshots/2024-02/light/0-home-light.png
  - url: /screenshots/2024-02/light/0-home-options-light.png
  - url: /screenshots/2024-02/light/0-home-widgets-light.png
  - url: /screenshots/2024-02/light/2-auth-dialog-light.png
  - url: /screenshots/2024-02/light/6-kb-toggle-light.png
  - url: /screenshots/2024-02/light/7-settings-cell-light.png
  - url: /screenshots/2024-02/light/7-settings-wifi-light.png
  - url: /screenshots/2024-02/light/9-dialer-light.png
  - url: /screenshots/2024-02/light/11-angelfish-light.png
  - url: /screenshots/2024-02/light/12-tokodon-light.png
  - url: /screenshots/2024-02/light/13-alligator-light.png
  - url: /screenshots/2024-02/light/14-clock-alarms-light.png
  - url: /screenshots/2024-02/light/14-clock-light.png
  - url: /screenshots/2024-02/light/15-calculator-light.png
  - url: /screenshots/2024-02/light/16-weather-light.png
  - url: /screenshots/2024-02/light/17-kasts-details-light.png
  - url: /screenshots/2024-02/light/17-kasts-light.png
2022-screenshots:
  - url: /screenshots/screenshot-2022-04-1.png
    name: "Homescreen"
  - url: /screenshots/screenshot-2022-04-2.png
    name: "App Drawer (Grid)"
  - url: /screenshots/screenshot-2022-04-3.png
    name: "App Drawer (List)"
  - url: /screenshots/screenshot-2022-04-4.png
    name: "Action Drawer (Expanded)"
  - url: /screenshots/screenshot-2022-04-5.png
    name: "Action Drawer (Minimized)"
  - url: /screenshots/screenshot-2022-04-6.png
    name: "Task Switcher"
  - url: /screenshots/screenshot-2022-04-7.png
    name: "Weather"
  - url: /screenshots/screenshot-2022-04-8.png
    name: "Calculator"
  - url: /screenshots/screenshot-2022-04-9.png
    name: "Calendar"
  - url: /screenshots/screenshot-2022-04-10.png
    name: "Clock"
  - url: /screenshots/screenshot-2022-04-11.png
    name: "Angelfish, a web browser"
  - url: /screenshots/screenshot-2022-04-12.png
    name: "Discover, an app store"
  - url: /screenshots/screenshot-2022-04-13.png
    name: "Plasma mobile homescreen"
  - url: /screenshots/screenshot-2022-04-14.png
    name: "Index, a file manager"
  - url: /screenshots/screenshot-2022-04-15.png
    name: "Koko, a photo library viewer"
  - url: /screenshots/screenshot-2022-04-16.png
    name: "Dialer"
  - url: /screenshots/screenshot-2022-04-17.png
    name: "Settings"
  - url: /screenshots/screenshot-2022-04-18.png
    name: "Elisa, a music player"
  - url: /screenshots/pp_camera.png
    name: Megapixels, a camera application

2020-screenshots:
  - url: /screenshots/plasma.png
    name: "Plasma mobile homescreen"
  - url: /screenshots/weather.png
    name: KWeather, Plasma mobile weather application
  - url: /screenshots/pp_calculator.png
    name: Kalk, a calculator application
  - url: /screenshots/pp_camera.png
    name: Megapixels, a camera application
  - url: /screenshots/pp_calindori.png
    name: Calindori, a calendar application
  - url: /screenshots/pp_kclock.png
    name: KClock
  - url: /screenshots/pp_buho.png
    name: Buho, a note taking application
  - url: /screenshots/pp_kongress.png
    name: Kongress
  - url: /screenshots/pp_okular01.png
    name: Okular Mobile, a universal document viewer
  - url: /screenshots/pp_angelfish.png
    name: Pix, another image viewer
  - url: /screenshots/pp_folders.png
    name: VVave, a music player
  - url: /screenshots/20201110_092718.jpg
    name: The hardware


menu:
  main:
    parent: project
    weight: 2
---

The following screenshots were taken in February 2024.

{{< screenshots name="2024-screenshots" >}}

### 2022

The following screenshots were taken in April 2022.

{{< screenshots name="2022-screenshots" >}}

### 2020

The following screenshots were taken in October 2020.

{{< screenshots name="2020-screenshots" >}}
