---
title: postmarketOS podcast
image:
 - picture.png
date: 2024-05-04T18:00:00Z
---

[<img alt="Plasma Mobile's lead developer" src="https://plasma-mobile.org/2024/05/04/postmarketos-podcast/picture.png">](https://cast.postmarketos.org/episode/40-Interview-Devin-Lin-Plasma-Mobile/)

Our friends at postmarketOS hosted Plasma Mobile's lead developer Devin Lin on their podcast. You can find it [on the postmarketos website](https://cast.postmarketos.org/episode/40-Interview-Devin-Lin-Plasma-Mobile/)