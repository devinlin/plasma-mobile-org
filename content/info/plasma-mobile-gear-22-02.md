---
version: "22.02"
title: "Plasma Mobile Gear 22.02"
type: info
date: 2022-02-09
---

In this release following repositories are marked as unstable,

- kclock
- krecorder
- qmlkonsole
- tokodon
- plasmatube
- khealthcertificate
- vakzination

Following independently released modules are also part of gear,

- plasma-mobile-sounds (v0.1)

Some of this repositories are in process of kdereview, and will be marked stable in upcoming releases.

# Changelog

{{< details title="alligator" href="https://commits.kde.org/alligator" >}}
+ Allow to filter read entries. [Commit.](http://commits.kde.org/alligator/4f5dece4911bbf6d7ad049984984bf402698f85f) 
+ Fix crash on refreshing. [Commit.](http://commits.kde.org/alligator/7e6f9350354376b81afd4b8185d14ced16f1ae5c) 
+ Add page to view all entries in one list. [Commit.](http://commits.kde.org/alligator/ff6c7ed98d775b1654a521b352a05534a97157b5) 
+ Refresh all feeds on startup. [Commit.](http://commits.kde.org/alligator/ba99e1361ff8a5c962d27376b985a0e8290d2aa0) 
+ Wrap entry title. [Commit.](http://commits.kde.org/alligator/6d6a3fcf3f5c5a341dc0d90345d4d82d2f7e27f5) 
+ Require CMake 3.16. [Commit.](http://commits.kde.org/alligator/1679a6647b2fb84a400df2b8b7731438855ba2db) 
+ Remove broken symlink. [Commit.](http://commits.kde.org/alligator/0acc54767b8f256eb438505e1dbe88cfd29ba60f) 
{{< /details >}}

{{< details title="angelfish" href="https://commits.kde.org/angelfish" >}}
+ Angelfish-webapp: Make sure to find the notification config file. [Commit.](http://commits.kde.org/angelfish/2673118b8b36b8a30ff53b8b6a9687ca5a470c58) 
+ Update rust crates. [Commit.](http://commits.kde.org/angelfish/cac5b11f4758116351554ef3b6a76f2b41ce2d62) 
+ HACK: Add workaround for Phosh so we at least don't display the desktop. [Commit.](http://commits.kde.org/angelfish/7946f4bc9310d546244e4400e14bdf83488ba311) 
+ Adblock: Don't trust the filter list hosts to return proper file names. [Commit.](http://commits.kde.org/angelfish/091c7e5f50cc61d7d52e1f0821a0226585a4f17d) 
+ UserAgent: Use a better default for isMobile. [Commit.](http://commits.kde.org/angelfish/8f94328747876515192e7b825a274b231b0cffc2) 
+ Desktop: Disable horizontal scrollbar in list view. [Commit.](http://commits.kde.org/angelfish/ab98666339367482171b13fbbf11da87881e1d29) 
+ Desktop: Fix dropdown override dialog. [Commit.](http://commits.kde.org/angelfish/c3274f1b769125345d40ba331cf934a79054d26a) 
+ Desktop: Allow loading sheets. [Commit.](http://commits.kde.org/angelfish/e8c31e11fd23b3896789a565fae1fcdb3f7674de) 
+ Desktop: Wrap history popup ListView in a ScrollView. [Commit.](http://commits.kde.org/angelfish/3a74c35a7ed7093bb1587eed00aa25fbd3bb28c7) 
+ Desktop: Disallow html injection in list items. [Commit.](http://commits.kde.org/angelfish/3ba1b9ffede8fa3df012a841a11355a450bbf958) 
+ Show a popup when the URL bar is focused. [Commit.](http://commits.kde.org/angelfish/a0d8dc7a63cb1fad87c7e81806373d5baae0dc38) 
+ Desktop: fix some issues with the bookmarks page. [Commit.](http://commits.kde.org/angelfish/b07f7eef28c6f6228a7eab437a474c5f4e25a571) 
+ Desktop: Add find in page feature. [Commit.](http://commits.kde.org/angelfish/a8aefc21ed833240c15588edd1daf21825335ef9) 
+ Desktop: Allow opening / closing the developer tools. [Commit.](http://commits.kde.org/angelfish/878e2810c8340889eb298bb53a9a85284895630e) 
+ Desktop: Allow adding webapps. [Commit.](http://commits.kde.org/angelfish/801ce97e3121d8824c039f720a9e7887a41e69c8) 
+ Flatpak: Update corrosion. [Commit.](http://commits.kde.org/angelfish/034317245d9d4cb2c77e786f410641be13071935) 
+ Update rust crates. [Commit.](http://commits.kde.org/angelfish/eecb0235d79f8df6fb4f914d4fb110760651954a) 
+ Mobile: Use titles style and single back button. [Commit.](http://commits.kde.org/angelfish/8f0b13a2fe5c2ca37ccdac005ce6aa88ebfece3f) 
+ Set desktop file name. [Commit.](http://commits.kde.org/angelfish/5d5ea802b9f115ae17159a35e8d981e5400c3c31) 
+ Settings: adblock: Add refresh button in addition to touch gesture. [Commit.](http://commits.kde.org/angelfish/eb8029d511ff9b2330a36a23ef02cd7c2b386181) 
+ Mobile: Use tab-close instead of window-close. [Commit.](http://commits.kde.org/angelfish/9641097ef89231878c2228bebb3fd390f916953f) 
+ Settings: Fix reference error. [Commit.](http://commits.kde.org/angelfish/d77f1e7129ea8d58cdac5a679371e04d638c4f34) 
+ Update appstream metadata with desktop screenshot. [Commit.](http://commits.kde.org/angelfish/9db31143e927e1b3b687a7d285126af71a20b0f0) 
+ Fix another QStringBuilder bug. [Commit.](http://commits.kde.org/angelfish/3074ff7cdba37d4f71a8690e8e8708d98f103b8d) 
+ Fix crash with new compiler settings. [Commit.](http://commits.kde.org/angelfish/4f4796449d473e37d6ce8c78b0a1313c4193e6ed) 
+ Bump minimum frameworks version to 5.90. [Commit.](http://commits.kde.org/angelfish/2a38227ffeb7eae86b5db92ed76d13cc5f2d3b84) 
+ Make angelfish compatible with new compiler settings. [Commit.](http://commits.kde.org/angelfish/351fe33234e42a9aee8ec78516dbb4daf1af8b93) 
+ Simplify navigation logic. [Commit.](http://commits.kde.org/angelfish/212ad75260d2ce0646175abfeb05e525b3e856f0) 
+ Add title to general settings. [Commit.](http://commits.kde.org/angelfish/33e96a4116e11b9647f1852312d297ee5e73b277) 
+ Use same settings infrastructure on mobile and desktop. [Commit.](http://commits.kde.org/angelfish/58b194a49ea3646cabf6a3a2ca536781a49d2e61) 
+ Unify settings on mobile and desktop. [Commit.](http://commits.kde.org/angelfish/ca790584bc12707ef6725271b593044b3c40afb4) 
+ Mobile: Move general settings away from overview. [Commit.](http://commits.kde.org/angelfish/cb498fdbbfd81ae71942b45991cf7833ea5610c3) 
+ Mobile: Move adblock switch to settings. [Commit.](http://commits.kde.org/angelfish/86c6cbd6efbb68a0d943c0888abb141db81dbade) 
+ Desktop: Use same url validation that we use for mobile. [Commit.](http://commits.kde.org/angelfish/8d51733c752e5ffd9a5d6fec101906fb31d0e373) 
+ Desktop: Limit width of the url bar. [Commit.](http://commits.kde.org/angelfish/65e40527a8e70b558222487676c3941682c6c744) 
+ Adblock: Fix wrong check. [Commit.](http://commits.kde.org/angelfish/93340ad0535b13b3edaf01463f501cd65d8c1885) 
+ Implement desktop UI. [Commit.](http://commits.kde.org/angelfish/260cde582f6a29d171586242a85eaeb1105f1501) 
+ Make adblock generated css rules override ones from the website. [Commit.](http://commits.kde.org/angelfish/d27b70c8fe894aa37e205018fc12a3d0b0d82af7) 
+ Fix displaying the page title in the tab switcher. [Commit.](http://commits.kde.org/angelfish/f8dd54e4a9dce92d746534e6d0514b4b2b2c6df0) 
+ Fix developer tools. [Commit.](http://commits.kde.org/angelfish/d839424ffc91e3b205b246e90c6fd08291e20af2) 
{{< /details >}}

{{< details title="audiotube" href="https://commits.kde.org/audiotube" >}}
+ Update ytmusicapi. [Commit.](http://commits.kde.org/audiotube/bfb41310ad6044420f103b34456364c9e6ddf695) 
+ Minor readme and gitignore updates. [Commit.](http://commits.kde.org/audiotube/2c0b13ff9fcaa0b35ab26605471ee6138029d2bd) 
+ Replace .license files with entries in dep5. [Commit.](http://commits.kde.org/audiotube/abfd901a82510c59aafb0c5f04846e7fb31b3a50) 
+ Fix typo in requirements.txt. [Commit.](http://commits.kde.org/audiotube/135b8859d611e987c7f8dcb32464af9ae0029c98) 
+ Flatpak: Update yt-dlp. [Commit.](http://commits.kde.org/audiotube/f8637b686cbeb60e03d7a52607d2d3f289d0ca83) 
+ Update tested ytmusicapi version to 0.19.5. [Commit.](http://commits.kde.org/audiotube/53f466c9767e40df58967d36ac605f3d3dda0f5a) 
{{< /details >}}

{{< details title="calindori" href="https://commits.kde.org/calindori" >}}
+ Fix calendar deletion and editing. [Commit.](http://commits.kde.org/calindori/670a13a306df5fd9031d9f03ec3269e3abe5741e) 
+ Fix CI attempt. [Commit.](http://commits.kde.org/calindori/383d0478e73f3479aa185bc8b5297d0395e10e93) 
+ Update CMakeLists.txt. [Commit.](http://commits.kde.org/calindori/a62783eea73bfa5de0bf0da1304867bde819a0c1) 
+ Use QStringLiteral and Q_EMIT. [Commit.](http://commits.kde.org/calindori/802f553c364516768c4ee4f4f269c0884a399fca) 
+ Update CI. [Commit.](http://commits.kde.org/calindori/56a0178397a0d6fca33c9d9c654073cf58bc725d) 
+ Update metadata. [Commit.](http://commits.kde.org/calindori/9b5c83cb6391744bc64c257b5b9652b68114f58f) 
+ Remove redundant files, use Kirigami.Dialog for confirmation. [Commit.](http://commits.kde.org/calindori/2a608ccce4b9f00fc5676b448ef1af1ee5e7ff7d) 
+ Use pagestack navigation and remove layer uses. [Commit.](http://commits.kde.org/calindori/61c16b69c79e0e731039e78b397ceae4f0ea2b49) 
+ Rework page actions to be in toolbar. [Commit.](http://commits.kde.org/calindori/13340229eb69f8a1dd48a797a8f2adc7c7cff924) 
+ Rework sidebar navigation and move calendar edits to settings page. [Commit.](http://commits.kde.org/calindori/b528a27e8f54f2ad0f3c25152bd162541c834698) 
+ Update README. [Commit.](http://commits.kde.org/calindori/8636ee84c4f6f94ae2e6ccc9c5a026bff037a2d0) 
+ Add repository icon. [Commit.](http://commits.kde.org/calindori/5d395e59d7d536beeecccf3782ea62cadd5e2061) 
+ Make LocalCalendar's calendar property a read-only raw pointer. [Commit.](http://commits.kde.org/calindori/8a4bd358630d8505755af4e876a66ccda5cb9444) 
+ Partly port from passing around calendar instance to using the CalendarController singleton. [Commit.](http://commits.kde.org/calindori/d8f63cf120ed3953698b100813aacf196b950a63) 
+ Expose active calendar in CalendarController. [Commit.](http://commits.kde.org/calindori/da1aa3c5046db4b8a267b99fee67202b61565603) 
+ Make CalendarController a C++ singleton. [Commit.](http://commits.kde.org/calindori/1ce6e313682dc5bcd14ba88de6bd1a80a03aea92) 
+ Make CalindoriConfig a C++ singleton. [Commit.](http://commits.kde.org/calindori/f428a03b07109a774271dc5b96c9eca27e96b5b8) 
{{< /details >}}

{{< details title="kalk" href="https://commits.kde.org/kalk" >}}
+ Update org.kde.kalk.desktop. [Commit.](http://commits.kde.org/kalk/e459d53643be07b6c4c7fcd93d61d49355a24d1e) 
+ Add Keywords for org.kde.kalk.desktop. [Commit.](http://commits.kde.org/kalk/6680a60ef061de1dbbb79646ee241009dea78116) 
+ Fix second row of calculation page positioning. [Commit.](http://commits.kde.org/kalk/412a5c2715f8730c586ae829db4774189e9f781c) 
+ Move opening history page to calculation page action. [Commit.](http://commits.kde.org/kalk/cfb90689627ed3ed4eee083b5e418de32b68e1ee) 
+ Improve font size calculation. [Commit.](http://commits.kde.org/kalk/635ae8750d985cea3be37bd3d56367bd73522a8b) 
+ Add blue ocean highlight style on keys. [Commit.](http://commits.kde.org/kalk/76100eb109180b29484001bde9665b38a152405a) 
+ Ensure window fits on phone landscape. [Commit.](http://commits.kde.org/kalk/94a60cfa2e729cd64e9c9bd3c17bc453ccf32545) 
+ Ensure that window fits on phone screens. [Commit.](http://commits.kde.org/kalk/735476fd4eee2066f2eb3287c7faaf728656b5a1) 
+ Reduced keypad and font size. [Commit.](http://commits.kde.org/kalk/c2882b0e731dc7f44add57967c305c9fda3bd228) 
+ Apply 1 suggestion(s) to 1 file(s). [Commit.](http://commits.kde.org/kalk/f5c10c1aa3b26ca4ca3c0ef05d53c262411140f2) 
+ Unit converter page keypad and font size change. [Commit.](http://commits.kde.org/kalk/f580d5baadb4eb597f6d0c8494e1c167c6ed18b9) 
+ Calculation page and binary page keypad and font size change. [Commit.](http://commits.kde.org/kalk/41f9f3ed8ee91979220b2a10524e8638a2546dd0) 
+ Hard coding number of columns in Function Pad. [Commit.](http://commits.kde.org/kalk/e6e9a775dc53438bbb8c864805d0bafd366fa205) 
+ Text overflow in button fix. [Commit.](http://commits.kde.org/kalk/879339293a8cd1393077c6fe70c0694765024cce) 
+ Set minimum height. [Commit.](http://commits.kde.org/kalk/5d74fb385f0d3bffc0b535c99daf2c4d34eb47bb) 
+ Rearranged trigonometric functions to be together. [Commit.](http://commits.kde.org/kalk/8aefc86d58f18c62803edb27bc9a1d344ec828a0) 
+ Update to KF89. [Commit.](http://commits.kde.org/kalk/ae3e849a26532f7464f86dcca5412a363ee17b4b) 
+ Add display length to metadata. [Commit.](http://commits.kde.org/kalk/8c98f1915b612b1fc4cb2f852383d15f5b5a2a88) 
{{< /details >}}

{{< details title="kasts" href="https://commits.kde.org/kasts" >}}
+ Re-enable icons for gpodder and nextcloud. [Commit.](http://commits.kde.org/kasts/448304690455dd8ee2054d19b5f35bc55cde745b) 
+ Avoid overflow on slider bar on very long podcasts. [Commit.](http://commits.kde.org/kasts/080669c28329e958d9b2e886085a3c3666eb2671) 
+ Optimize placement of mobile player image and title for landscape mode. [Commit.](http://commits.kde.org/kasts/bd46d33863405df9f5862af27a85b84b5a2fce4d) 
+ Optimize placement of info messages (take into account sync message). [Commit.](http://commits.kde.org/kasts/8e0b346d5f25d5ee5df54f38fe7caadf59c462d2) 
+ Optimize placement of info and error messages. [Commit.](http://commits.kde.org/kasts/6c51086749d4b3719877758941701dab8d0fd24c) 
+ Don't attempt to change globaldrawer properties if it's not loaded yet. [Commit.](http://commits.kde.org/kasts/3aa45e555af72a3b5febeaf307f9c655d9f311ac) 
+ Keep fetching episode actions until we get the current timestamp. [Commit.](http://commits.kde.org/kasts/91aa4159080901104dc5364dfab7a49ad6cb6049) 
+ Apply Kirigami Theme color for TextEdit explicitly. [Commit.](http://commits.kde.org/kasts/5380a431e2facd5e34f36de78514525040ed0c33) 
{{< /details >}}

{{< details title="kclock" href="https://commits.kde.org/kclock" >}}
+ Fix incorrect timezone add order. [Commit.](http://commits.kde.org/kclock/fb630af18e41a6183f3c1e7ebfa5aca6fccd0268) 
+ Ensure kclockd is always up before the program executes. [Commit.](http://commits.kde.org/kclock/0dbd3953339e54e1e8b0512b1057037f66b42245) 
+ Update org.kde.kclock.desktop. [Commit.](http://commits.kde.org/kclock/75ece8c2f3d478755fe89561e6ae7f11764121a8) 
+ Reconnect signals if kclockd was not up. [Commit.](http://commits.kde.org/kclock/51dc351e37d571fb80ed58dcdc55f5a7bb40308a) 
+ Fix alarm ring duration display. [Commit.](http://commits.kde.org/kclock/b28eb4291606eb619869d9d9a26c35c3ce1fd9a9) 
+ Fix alarms not working on pinephone. [Commit.](http://commits.kde.org/kclock/c0c8b90e5a95e8ba06a122126c77c3481432d403) 
+ Fix list delegate dragging. [Commit.](http://commits.kde.org/kclock/5b1f5ef3177daaf5be0b713e2c52031191df6205) 
+ Fix warnings and layers. [Commit.](http://commits.kde.org/kclock/d0fce355b8198533401d3fac2de819126550e223) 
+ Use page stack instead of layers for gestures. [Commit.](http://commits.kde.org/kclock/420118de9762aff2d5c200025322a256f7e0135a) 
+ Move from sheet to full page for add location component. [Commit.](http://commits.kde.org/kclock/4fa9a3cbfe4a1a39c99a9cb937b4068ad159b829) 
+ Cleanup timers and add animations. [Commit.](http://commits.kde.org/kclock/e8f65d5abb4303311b7113334e6cbd35fc52fd8b) 
+ Add separate edit and add views for time page. [Commit.](http://commits.kde.org/kclock/2588ba9e1ab9062b520025d0347b0b1c1ff82125) 
+ Remove unused code in kclockformat. [Commit.](http://commits.kde.org/kclock/75b7dce6434d82e22c8b3d2adbce6d6d51d2e306) 
+ Add time format setting and remove alarm volume. [Commit.](http://commits.kde.org/kclock/bd0588e27dac4cbf8cfc3d3fb513aca322678196) 
+ Add list separator and refactor sound page. [Commit.](http://commits.kde.org/kclock/1c050ef5d855c212584e15c297f53cbb57b24471) 
+ Fix wrong padding on sidebar when different qqc2 themes are used. [Commit.](http://commits.kde.org/kclock/425268d4c89b623500423b2cd72deaa337b61d68) 
+ Add auto generated files to .gitignore. [Commit.](http://commits.kde.org/kclock/9208d5316641aace6fced6ab0644f69ad6a356a4) 
+ Fix infinite loop in kclockd. [Commit.](http://commits.kde.org/kclock/d6627b5d18022e2dfa2cef9b27f4a0f54bf30166) 
+ Fix audio not playing in sound picker if radio button is checked. [Commit.](http://commits.kde.org/kclock/1f12d0f3f2014dcb20ee3e6bfd8778aca0102dfa) 
+ Fix sound picker and displayed text. [Commit.](http://commits.kde.org/kclock/2969a4d5a9d1d6c9c8f1ae224858adec23d5b2ac) 
+ Fix sound picker margins. [Commit.](http://commits.kde.org/kclock/ad529cd8e358bb959b3a89145c52f613628315dc) 
+ Fix comparison. [Commit.](http://commits.kde.org/kclock/677324d54d2eb3e470a62470754268160bbcb5f6) 
+ Add proper sound selection support for alarms. [Commit.](http://commits.kde.org/kclock/169cfe771b4b5fe36a065113fa729318b06c4676) 
+ Use Dialog for timezone sheet. [Commit.](http://commits.kde.org/kclock/bd3a0d1fd3c5dc40c8d13be3d3ac1aba8e3118b2) 
+ Rework alarm list page to have an edit mode. [Commit.](http://commits.kde.org/kclock/3e735a94f7414872a9a98fd6f04ca95f16dda8c6) 
+ Fix snooze length not being reset. [Commit.](http://commits.kde.org/kclock/a93f6677b2fa0c77b47cf56068764ce694e0f09d) 
+ Start snooze only after the snooze button is pressed. [Commit.](http://commits.kde.org/kclock/39fd632a120f607fd5f3742ee45d60cbbc8e12a7) 
+ Add in-app alarm dialog when ringing. [Commit.](http://commits.kde.org/kclock/9d5fab30fd464d3194708acd6d537cf58ffea2f4) 
+ Check timer form duration buttons if the value is the same. [Commit.](http://commits.kde.org/kclock/e0dccef4f0a1a8d9e777a5ec952a81ae7a3caf3e) 
+ Use Kirigami.Dialog for timer form. [Commit.](http://commits.kde.org/kclock/b7d330ba9343c56d9a679728d88e51da09c10233) 
+ Fix missing license header. [Commit.](http://commits.kde.org/kclock/17e1a39b8000ce48dbeb5eece24c1aac3f8f77cc) 
+ Refactor kclockd alarms and QML, and add snooze length + ring length settings per alarm. [Commit.](http://commits.kde.org/kclock/23b1e2dfcf9df46b8c683218f69a049c8c70ab07) 
+ Reorganize qml files. [Commit.](http://commits.kde.org/kclock/d20f96f56e966592a5ee916108e001615fe5cffa) 
+ Add sound picker component. [Commit.](http://commits.kde.org/kclock/b04256cb30ede093f990e69d3695f1804d25aa7f) 
+ Refactor and update code to update KF5 dependency to 5.89. [Commit.](http://commits.kde.org/kclock/41a097d909c7f1e87bd9eb54ada0919617ce4a14) 
+ Switch to Kirigami.Dialog. [Commit.](http://commits.kde.org/kclock/1cf7fd19ef6896cdf57dee0d23afac89a9a74dac) 
+ QML cleanup and remove replace uses of appwindow with applicationWindow(). [Commit.](http://commits.kde.org/kclock/b25f1639dcc0be630401c1c56f763efc691abf1e) 
+ Update metadata. [Commit.](http://commits.kde.org/kclock/66ceedef269fc44147600a49f92e21127b0f53e1) 
+ Fix lightened screenshots. [Commit.](http://commits.kde.org/kclock/596df24be9bd2c7c471d97b3d3b084fddd95cf18) 
+ Fix wrong DBus object name. [Commit.](http://commits.kde.org/kclock/124d310d7c78dd88e5327095714277e1caf08a25) 
{{< /details >}}

{{< details title="keysmith" href="https://commits.kde.org/keysmith" >}}
+ Reduce default size of main window. [Commit.](http://commits.kde.org/keysmith/e8c2d5c2cf9811cca8522d709c135187c25cbc42) 
+ Focus password field by default on launch. [Commit.](http://commits.kde.org/keysmith/94d3eb3c13dc635e2f84bcf54057423cdfde3b35) 
{{< /details >}}

{{< details title="khealthcertificate" href="https://commits.kde.org/khealthcertificate" >}}
+ Adapt unit test to the by now expired sample certificate. [Commit.](http://commits.kde.org/khealthcertificate/32e9e01220c208348327e1939fbd051d86e4ed47) 
+ Attempt to fix the Windows build. [Commit.](http://commits.kde.org/khealthcertificate/a9f95a1192c5b819ef0ebc306abd96a6dd2169af) 
+ Update certificates and data files. [Commit.](http://commits.kde.org/khealthcertificate/950d2e3ae4d10881d2ccc8917edadfc37841e22d) 
{{< /details >}}

{{< details title="koko" href="https://commits.kde.org/koko" >}}
+ Add auto generated files to .gitignore. [Commit.](http://commits.kde.org/koko/c447d10fdc8a90b9c097955946fe6376448368f5) 
+ Add fileinfo basic tests and enable ci. [Commit.](http://commits.kde.org/koko/0e60568806bcc976fcdf1064f7dfbc3166acaeec) 
+ Raise window on activation. [Commit.](http://commits.kde.org/koko/1701509f26c1bb9fdfea637e78ad8b132df8226b) 
{{< /details >}}

{{< details title="kongress" href="https://commits.kde.org/kongress" >}}
+ Ensure that export message is always shown. [Commit.](http://commits.kde.org/kongress/8cace2b2ea90bf0791dfcc412ae1164feda2df68) 
{{< /details >}}

{{< details title="krecorder" href="https://commits.kde.org/krecorder" >}}
+ Add dialog opening delay when opened from context menu. [Commit.](http://commits.kde.org/krecorder/997498536de06b26326d280e125772b3c8f4fe63) 
+ Ensure context menu only opens with a mouse. [Commit.](http://commits.kde.org/krecorder/8460ac9c8013964d4af8dc598fa879443c457711) 
+ Fix dialogs closing immediately when opened from context menus. [Commit.](http://commits.kde.org/krecorder/f0c432fefd5750e9026e83b39951f6366f42fe8d) 
+ Remove recordings that don't exist. [Commit.](http://commits.kde.org/krecorder/148c6532b11935aba78d07c62aeca7def4f8e15d) 
+ Fix row insert signal. [Commit.](http://commits.kde.org/krecorder/e3629e9389e29f4749a6d3faf1c8e619ac005241) 
+ Swap discard and save button locations. [Commit.](http://commits.kde.org/krecorder/1c3d742fb720f1f2f9fdc4c7cabf293b8826f9ce) 
+ Fix AndroidManifest.xml. [Commit.](http://commits.kde.org/krecorder/9d2f74af9e5dfcaa535d0a7296bd2055280aa859) 
+ Capture mouse events behind menu. [Commit.](http://commits.kde.org/krecorder/ef475f97d8927e4852cf76fa641098afbf6c45e7) 
+ Add edit mode and right click context menu. [Commit.](http://commits.kde.org/krecorder/6fdc44ee1411932f5d8f39479dfed08257e27939) 
+ Make REUSE compliant and add check to ci. [Commit.](http://commits.kde.org/krecorder/cd515639f6eb64e01efb8774565f8ace29f77616) 
+ Add kcoreaddons to .kde-ci.yml. [Commit.](http://commits.kde.org/krecorder/a7cd832a601424cc1f019786a120351dc176be72) 
+ Fix recording component not loading. [Commit.](http://commits.kde.org/krecorder/98345a27705fc670b69eb09d969d1d49343b98be) 
+ Refactor recording model and use proper QAbstractListModel. [Commit.](http://commits.kde.org/krecorder/d0976d81981633be291682d1dd8d63500dbe3643) 
+ Add KAboutData. [Commit.](http://commits.kde.org/krecorder/7e5d4e52d0876c535064593da063e556f948c6bd) 
+ Update to KF89. [Commit.](http://commits.kde.org/krecorder/cdc2a3c59be0877458208ccbb584a1750aefac8f) 
+ Add auto generated files to .gitignore. [Commit.](http://commits.kde.org/krecorder/896743d025f00c470c31343a63db77cbe6e32080) 
+ Update metadata with display length. [Commit.](http://commits.kde.org/krecorder/8a0f0ccaca902b484c5ef0a6d7a77122c8e1bc46) 
{{< /details >}}

{{< details title="ktrip" href="https://commits.kde.org/ktrip" >}}
+ Fix APK creation. [Commit.](http://commits.kde.org/ktrip/95f6a7b3a7377e0de35d94beef7ec870d74154aa) 
+ Make KTrip fully REUSE compliant. [Commit.](http://commits.kde.org/ktrip/043d04c02243d8cb11e138e7be43165106336de7) 
+ Use the new KI18n country localization API. [Commit.](http://commits.kde.org/ktrip/dd83cfe31fe9fead8ca08bec9ae05228f7945900) 
+ Remove duplicate entry from .gitignore. [Commit.](http://commits.kde.org/ktrip/e5d230bb7c235378589259d060ffdffb3db39eaf) 
{{< /details >}}

{{< details title="kweather" href="https://commits.kde.org/kweather" >}}
+ Update org.kde.kweather.desktop. [Commit.](http://commits.kde.org/kweather/044015e92012ad153c546846dd761919f2b20f84) 
+ Move side bar functions to the toolbar and setting minimum window size. [Commit.](http://commits.kde.org/kweather/5987ba009114ff73ae2fe193a11da90f06d5aba8) 
+ Fix current weather being updated with selected day changing. [Commit.](http://commits.kde.org/kweather/a1493a72397d897a7ad84f5d99be347548c870f4) 
+ Update README. [Commit.](http://commits.kde.org/kweather/2d84e5ef4c6982f3286d3a3b94dc0ff626f693cf) 
+ Update metadata description and screenshot. [Commit.](http://commits.kde.org/kweather/45966ba69ff65070397614913bb53b023e2f41d2) 
+ Remove kquickcharts dep from CI. [Commit.](http://commits.kde.org/kweather/1f05992f496a72f323919e74048e76cf82b9f4f0) 
{{< /details >}}

{{< details title="neochat" href="https://commits.kde.org/neochat" >}}
+ Don't recreate config group when saving last room. [Commit.](http://commits.kde.org/neochat/3b73409b7abd4881e882dd4ff3eab506a47c7e4a) 
+ Don't crash on empty creation event. [Commit.](http://commits.kde.org/neochat/335ef240f54e306176f30eccab3da88589c5631d) 
+ Implement sharing with Purpose (export). [Commit.](http://commits.kde.org/neochat/ca8a21c0eb17503f564e706c3e9eed3e10e60677) 
+ Use ellipsis in «Loading…» strings. [Commit.](http://commits.kde.org/neochat/3e6f38c8ea0d36657826ff6cff99b67b8967e86e) 
+ Implement adding labels for account. [Commit.](http://commits.kde.org/neochat/a6ab447955edb5890fa8879aaf1f484d78e89bad) 
+ Immediately apply leave/join event setting. [Commit.](http://commits.kde.org/neochat/0b7dcd70acfdc55969c7c66da049afdf90ff910e) 
+ Fix left margin in EncryptedDelegate. [Commit.](http://commits.kde.org/neochat/bce560b03b5b092b6f281de34aa2114f27efa212) 
+ Set empty state key for room avatar change events. [Commit.](http://commits.kde.org/neochat/5a1198d28cf23d492a85ccfdcfb0cdf021af41bb) 
+ Give settings window a title. [Commit.](http://commits.kde.org/neochat/6ac6234886d0a9f712d6655c094c0ff2e17283f5) 
+ Reduce minimum height of the window. [Commit.](http://commits.kde.org/neochat/fc9e4fc961a1d71ae17cf833ccc56a7a5f4fb707) 
+ Modernize code to activate window on user activation. [Commit.](http://commits.kde.org/neochat/24644887e0d44db79235f7537c6b0ee84f22ff00) 
+ Use the x-kde-origin-name notification hint to pass the account name to push notifications. [Commit.](http://commits.kde.org/neochat/a29ec0a18a29ff6314d0efa12117da13cb675318) 
+ Fix build with qcoro 0.4. [Commit.](http://commits.kde.org/neochat/9300e65239f9204a688084af20d05138d272fdb4) 
+ Change X-GNOME-SingleWindow key to SingleMainWindow. [Commit.](http://commits.kde.org/neochat/ee59006c08f8e350939a6703210f606c14c35d19) 
+ Don't set cmake policy when using libquotient 0.7. [Commit.](http://commits.kde.org/neochat/ca8702fd5e2f972fa4124af005b81b38f33b8faf) 
+ Set bugs url. [Commit.](http://commits.kde.org/neochat/183c3227a97e31c974804cda42236285d9a3d1f7) 
+ Raise windows also on other platforms. [Commit.](http://commits.kde.org/neochat/200281702a9c06a430ad56427ecbc04fb27f2859) 
+ Fix issues with saveFileAs. [Commit.](http://commits.kde.org/neochat/297684a139ee91021bd9ffe27d28afc32fb2b41f) 
+ Changed "Settings" to "Configure NeoChat" in menu. [Commit.](http://commits.kde.org/neochat/aeee367e8216c452581637d8d95e092e492af6af) 
+ Fix variable lookup in the timeline delegates. [Commit.](http://commits.kde.org/neochat/aa9dcc87cb09e6b8b322fe02f452dc15d883ef64) 
+ Improve toolbar on mobile. [Commit.](http://commits.kde.org/neochat/8a70e240e44b16ffda90979541db0f452536d359) 
+ Ifdef version for compatibility with our minimal required version. [Commit.](http://commits.kde.org/neochat/50a7df8e031aac160151fb2c0d3c006e36d5bc75) 
+ Improve emoji pane. [Commit.](http://commits.kde.org/neochat/dd977976db80d0298a228e8e9baa11ed9135f9d7) 
+ Check if password can be changed. [Commit.](http://commits.kde.org/neochat/de666b93770ef083bc6fb73c410e75c36180f95e) 
+ Fix image tooltip. [Commit.](http://commits.kde.org/neochat/5f413782147f271248bb4ac3116fa65a38e4efb2) 
+ Expose hiddent GUI option. [Commit.](http://commits.kde.org/neochat/383b31c185ed08901594f7633a575f72c18bea78) 
+ Adapt list setting pages to new style. [Commit.](http://commits.kde.org/neochat/de2fbadba5ec60c72466aa47a2b095a52e984438) 
+ Allow using ESC to go back to room list. [Commit.](http://commits.kde.org/neochat/67bc66ee0ce9e16d8eacc9ba60d48ded6f8982e7) 
+ Port away from QNetworkConfigurationManager. [Commit.](http://commits.kde.org/neochat/924a1fed213a456f168c6dc281b612faf1025add) 
+ Fix reuse issue. [Commit.](http://commits.kde.org/neochat/b0a1de757207cd8f095d887db515c947645c40f9) 
+ Remove lag when starting user autocompletion. [Commit.](http://commits.kde.org/neochat/ca2b5fde8e16e90e60980db2f42171d367cffb18) 
+ Set single window hint in desktop file. [Commit.](http://commits.kde.org/neochat/26f0cd4cf4df9a9a41ad8b1cca5525c5ae27486c) 
+ Make room address selectable. [Commit.](http://commits.kde.org/neochat/0801b815c8cadf8479ed89c179421db8791a8173) 
+ Display monochrome icon in tray. [Commit.](http://commits.kde.org/neochat/28137c8c86623947d6bb43b566570307179964b1) 
+ Fix QuickSwitcher activation. [Commit.](http://commits.kde.org/neochat/e79e06235f346e498fbfd512bb868afd8870399d) 
+ Fix Windows/mac build. [Commit.](http://commits.kde.org/neochat/dce7fde7a64bdbfe12de6b49d89c210e31772ab1) 
+ Revert "Add CI for FreeBSD". [Commit.](http://commits.kde.org/neochat/8d9f3b86583ed5cf1f19ae46893a23ef527467ea) 
+ Fix notifications on Android. [Commit.](http://commits.kde.org/neochat/5e1adf7ea7c4230115aac7244476284aa2fbf73e) 
+ Add CI for FreeBSD. [Commit.](http://commits.kde.org/neochat/d71ccc46d0495934f0b840e2cc723ee21b2f890b) 
+ Support raising when we receive a notification. [Commit.](http://commits.kde.org/neochat/284a1734ae98fbf18d4fb73db35ca47bae0a0f98) 
+ Remove unused function. [Commit.](http://commits.kde.org/neochat/8722c99c932e08c87b95d263a110ab67b887410a) 
+ Use a reasonable role for message source. [Commit.](http://commits.kde.org/neochat/0c5932b3dadb0cc611759de8ea36c308228f0ee5) 
+ Minor improvements. [Commit.](http://commits.kde.org/neochat/332d6c9782eb190779958d89d528b54c4b28fceb) 
+ Don't connect to something that isn't a signal. [Commit.](http://commits.kde.org/neochat/91f3f64bb5b490cd27c158bf102c76ec55e7c22d) 
+ Refactor delegates. [Commit.](http://commits.kde.org/neochat/599ab11656aedf14e92b28d634d66157ef475fa8) 
+ Remove dead code. [Commit.](http://commits.kde.org/neochat/ff707b7a58de9215d207ccd5965d616d57917f23) 
+ Don't render html in RoomDrawer heading. [Commit.](http://commits.kde.org/neochat/e551319245e8a560646f88cecf5472da39a16f4a) 
+ Add support for minimizing to system tray on startup. [Commit.](http://commits.kde.org/neochat/59430cce89471cdbcb3abd870a9fc1ca5883998b) 
+ Use non blocking passord reading. [Commit.](http://commits.kde.org/neochat/d1bbb5e3f7269c4654e347e0ff2603c7e938dab6) 
+ Add a mobile oriented context menu for the room list. [Commit.](http://commits.kde.org/neochat/6e1c07047e7a85b37212b9bf73e1aa6a186390ad) 
+ Fix loading room settings on mobile. [Commit.](http://commits.kde.org/neochat/738270f513741de4624e3a5c4facc4564051cfd1) 
+ Use icon from qrc for system tray icon. [Commit.](http://commits.kde.org/neochat/16d43e9ee8a2ef9c25a5aa063abf637120e075a7) 
+ Adapt to libQuotient API change. [Commit.](http://commits.kde.org/neochat/d0e04e0c97e5b82a0469482ca6d83b2dd7f255e4) 
+ Prevent instability with TextArea with null as background. [Commit.](http://commits.kde.org/neochat/658eb187c999aca98179a9e145df7dbf9a8a9ca6) 
{{< /details >}}

{{< details title="plasma-dialer" href="https://commits.kde.org/plasma-dialer" >}}
+ [kde-telephony-daemon] Pause media players on incoming call. [Commit.](http://commits.kde.org/plasma-dialer/a594e41fe922e49440222c908c2275c3919e2601) 
+ [plasma-dialer] Support choosing of phone number via contacts. [Commit.](http://commits.kde.org/plasma-dialer/0173c3f83d5145b6369a4c2d8570d9735bb4ad0f) 
+ [kde-telephony-plugin-declarative] ContactUtils: introduce phoneNumbers. [Commit.](http://commits.kde.org/plasma-dialer/537fcf295eb41e1b9a9f7a59ba8d08f11b92e602) 
+ [plasma-dialer] Return to dialer page after a call. [Commit.](http://commits.kde.org/plasma-dialer/8259ced2c726edfc5aa8e886a4d41d137dc0af6d) 
+ [kde-telephony-daemon] Restore audio profiles switching. [Commit.](http://commits.kde.org/plasma-dialer/2294e44d9c7b4aab0dcfa13a6b22a8ddce4c5cf4) 
+ [plasma-dialer] Introduce promt dialog to clear history. [Commit.](http://commits.kde.org/plasma-dialer/7d7c99e6235093ef5f6d36b770069aa356109096) 
+ Update KF5 minimal version. [Commit.](http://commits.kde.org/plasma-dialer/b228af2a82a541b88b1f1b15f39e36afd1355ad0) 
+ Add settings and about pages, and improve page navigation. [Commit.](http://commits.kde.org/plasma-dialer/d097b9b1a56c27357261657705f12f40fb373e7a) 
+ Update sidebar button style to plasma style. [Commit.](http://commits.kde.org/plasma-dialer/c96020b223215ef8967fe5bf94065514a493ff67) 
{{< /details >}}

{{< details title="plasma-settings" href="https://commits.kde.org/plasma-settings" >}}
+ Remove unneeded X-Plasma* definitions in json metadata. [Commit.](http://commits.kde.org/plasma-settings/c97b4ce7972b5773c6e1c6d0e1f0737340a99191) 
+ Remove unneeded EnabledByDefault definitions in json metadata. [Commit.](http://commits.kde.org/plasma-settings/0da8abb8b7b4865783cfe6fd2dd6d38e79749e82) 
+ Remove unneeded X-KDE-ParentApp definitions in json metadata. [Commit.](http://commits.kde.org/plasma-settings/d62ad812be6ace83550f731e5064008814ca8a34) 
+ Remove unneeded ServiceType definitions in json metadata. [Commit.](http://commits.kde.org/plasma-settings/c7317e52460ae2738fcdb49ddacc6a1572ef40ba) 
+ Remove unneeded plugin ids from json metadata. [Commit.](http://commits.kde.org/plasma-settings/f7b988a2863bc9066eff16bea16e84ede0f995e4) 
+ Module: Output warning if one tries to set the path to an invalid metadata object. [Commit.](http://commits.kde.org/plasma-settings/a774a7f8329fc5ba1d82e3a9b4563f569f7197ef) 
+ Module: Add name property for source code compat of plasma-bigscreen. [Commit.](http://commits.kde.org/plasma-settings/2bd8d0b9e98c32a8a8474f5f0c2e7c4950299cba) 
+ Revert "Revert "Port KCM querying from KPackageLoader to KPluginMetaData"". [Commit.](http://commits.kde.org/plasma-settings/b0e81ae5ca2269e366c8a8af6849b3fbae6da72a) 
+ De-duplicate metadata for KPackages & convert files to json. [Commit.](http://commits.kde.org/plasma-settings/221484214146e83f452a6e8d54a2e8c96bd6e73b) 
+ [vkbd] Improve FormLayout titles, add text wrapping and keyboard testing. [Commit.](http://commits.kde.org/plasma-settings/211395a9b26be63bd5ac30756d5f1dfa2e451f52) 
+ Properly define the spacing and padding in the sidebar. [Commit.](http://commits.kde.org/plasma-settings/e3688b5e62c393e2eea5ca4427b2c8816cbd0f80) 
+ Add repository icon. [Commit.](http://commits.kde.org/plasma-settings/7a8be387bb1ecabf49a8e1af84038c6993b87166) 
+ Fix module activation. [Commit.](http://commits.kde.org/plasma-settings/b4b3cfb025a635859f1ab9b0168b420162cfb4c2) 
+ Add search, titles toolbar style, and a widescreen sidebar. [Commit.](http://commits.kde.org/plasma-settings/0e750c121e3933cec289f5833beb909b759f3333) 
{{< /details >}}

{{< details title="plasmatube" href="https://commits.kde.org/plasmatube" >}}
+ Rework app navigation to use NavigationTabBar. [Commit.](http://commits.kde.org/plasmatube/e11a6991049c77ed4a9dd2b419f99eb1a95b64b4) 
{{< /details >}}

{{< details title="qmlkonsole" href="https://commits.kde.org/qmlkonsole" >}}
+ Fix tab index not updating properly. [Commit.](http://commits.kde.org/qmlkonsole/773dd8150728c12986191a4eedd7749b87c76a59) 
+ Fix terminal text input focus being stolen at startup. [Commit.](http://commits.kde.org/qmlkonsole/bcee26204253ea3176632edc287ae7354c4f3a2b) 
+ Add repository icon. [Commit.](http://commits.kde.org/qmlkonsole/06ba226fd764deb3b3a5a99c21566c7b702c1958) 
+ Fix tab deletion. [Commit.](http://commits.kde.org/qmlkonsole/4b1b5f9dbea6a78185fd34d40bcd1d1bed19da4c) 
+ Ensure implicitWidth is factored in for ctrl and alt keys. [Commit.](http://commits.kde.org/qmlkonsole/cedf4e49cd644781ffc24aad4c6296d43ca32ca3) 
+ Fix swipeview interaction. [Commit.](http://commits.kde.org/qmlkonsole/31146dc5fc794b4d13613c4586eaa0cc58ce3016) 
+ Re-implement dragging and selection mode with handlers. [Commit.](http://commits.kde.org/qmlkonsole/35bd50261739c1700ab4b2a9ee1046491e48d9cd) 
+ Simplify code. [Commit.](http://commits.kde.org/qmlkonsole/9f14396105fa74589cb8373d040a68de390abf24) 
+ Revert accidental change to CMakeLists.txt. [Commit.](http://commits.kde.org/qmlkonsole/d73b0a04e6152a4dd97df84f3a51d3dbda90e87a) 
+ Add support for modifier keys (ctrl and alt). [Commit.](http://commits.kde.org/qmlkonsole/1203d1eee588a4111419ae806279d951b0a26cad) 
{{< /details >}}

{{< details title="spacebar" href="https://commits.kde.org/spacebar" >}}
+ Fix failed assert when messaging non-contact. [Commit.](http://commits.kde.org/spacebar/34f07473d76159c293aa82b1dfa4fcfb1f67b420) 
+ PhoneNumber: Store string if the number was not valid. [Commit.](http://commits.kde.org/spacebar/e7c577f7121188f51088a96f61074ba0fb32518c) 
+ Add gitlab CI. [Commit.](http://commits.kde.org/spacebar/07bdacc219404a5bfd31ac6d465cda1eb37a4a27) 
+ Refactor text input and make buttons slightly larger. [Commit.](http://commits.kde.org/spacebar/45675482821b02bc105233289188e23e4752a6bc) 
+ Remove unneeded semicolon. [Commit.](http://commits.kde.org/spacebar/acc779c6214b47e6eb5c8bd55a5a36b01fabdcd6) 
{{< /details >}}

{{< details title="tokodon" href="https://commits.kde.org/tokodon" >}}
+ Fix mobile sidebar handle icon, and show sidebar toggle on desktop. [Commit.](http://commits.kde.org/tokodon/fb8334c8ab47442b3493ddafbacaaf1128f7b73f) 
+ Update org.kde.tokodon.desktop - sed -i 's/SocialMedia/X-SocialMedia/g'. [Commit.](http://commits.kde.org/tokodon/12dd67580dc53736ba7de7bb5f5c50049a44b6e7) 
+ Fix uploading files. [Commit.](http://commits.kde.org/tokodon/751c2dab2d4c90883847209697f0dd69f813f31a) 
+ Add opt-in option to see detailed posts stats. [Commit.](http://commits.kde.org/tokodon/1920d2766f9ced6176246a57e5a2b867f0cae019) 
+ Const-ify more stuff. [Commit.](http://commits.kde.org/tokodon/a28b90ac04e7022722895f349a1074e9aabfd1a4) 
+ Fix removing accounts. [Commit.](http://commits.kde.org/tokodon/243ec2836f635fafe24f3be16071244f0026ff3c) 
+ Cmake: Correct minimum required version to fix ecm warning. [Commit.](http://commits.kde.org/tokodon/e969e35fd3388880b4eec99c56ead2afc7ecea09) 
+ Don't allow removing login page. [Commit.](http://commits.kde.org/tokodon/5f6be56bd57042abf62354345797506060a477d9) 
+ Don't allow posting empty post. [Commit.](http://commits.kde.org/tokodon/91f7b814dde099487061ec66c4d1d800e1902d47) 
+ Account: Replace raw ptrs with shared_ptr. [Commit.](http://commits.kde.org/tokodon/f50782b3ef83c4c6693acacb59dfc69e6f403f50) 
+ Fix some clazy warnings. [Commit.](http://commits.kde.org/tokodon/f409de5eda0d5dc793102d3007c784b19253d551) 
+ AuthorizationPage: Don't allow to continue with empty token. [Commit.](http://commits.kde.org/tokodon/54a483fba261320e121fd17db52f8ca38edfe7d7) 
+ AuthorizationPage: Continue on enter pressed. [Commit.](http://commits.kde.org/tokodon/3891d201cfd64cf2cb0feb4f28b7b799bed108b3) 
+ AuthorizationPage: Use correct cursor when hovering link. [Commit.](http://commits.kde.org/tokodon/43311e16038ff196d792fb1bc07fdca850befe0b) 
+ LoginPage: Don't allow to continue with empty username/instance url. [Commit.](http://commits.kde.org/tokodon/cb2ca723947db7f0c14d74503b66985395104622) 
+ LoginPage: Jump to next field on enter. [Commit.](http://commits.kde.org/tokodon/b3d02d7311069652a0cc886d9546846ec314b2f3) 
+ Gitignore: Add CMakeLists.txt.user and compile_commands.json. [Commit.](http://commits.kde.org/tokodon/a195bd16291294a3a858aca3d7acb01fe8c0d211) 
{{< /details >}}

